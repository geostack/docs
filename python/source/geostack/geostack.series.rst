geostack.series package
=======================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   geostack.series.test

Submodules
----------

geostack.series.series module
-----------------------------

.. automodule:: geostack.series.series
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: geostack.series
   :members:
   :undoc-members:
   :show-inheritance:

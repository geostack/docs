geostack.readers package
========================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   geostack.readers.test

Submodules
----------

geostack.readers.gribReader module
----------------------------------

.. automodule:: geostack.readers.gribReader
   :members:
   :undoc-members:
   :show-inheritance:

geostack.readers.ncutils module
-------------------------------

.. automodule:: geostack.readers.ncutils
   :members:
   :undoc-members:
   :show-inheritance:

geostack.readers.rasterReaders module
-------------------------------------

.. automodule:: geostack.readers.rasterReaders
   :members:
   :undoc-members:
   :show-inheritance:

geostack.readers.timeutils module
---------------------------------

.. automodule:: geostack.readers.timeutils
   :members:
   :undoc-members:
   :show-inheritance:

geostack.readers.vectorReaders module
-------------------------------------

.. automodule:: geostack.readers.vectorReaders
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: geostack.readers
   :members:
   :undoc-members:
   :show-inheritance:
